#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec  5 12:26:31 2017

@author: amir
"""

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Dense, Input, GlobalMaxPooling1D
from keras.layers import Conv1D, MaxPooling1D, Embedding
from keras.models import Model

import pandas as pd
import numpy as np
from sklearn.preprocessing import label_binarize
from collections import Counter
import os
import sys
import spacy

BASE_DIR = ''
GLOVE_DIR = os.path.join(BASE_DIR, 'glove42B')
MAX_NB_WORDS = 20000
MAX_SEQUENCE_LENGTH = 100
EMBEDDING_DIM = 300
VALIDATION_SPLIT = 0.2

def tokenize(tokens,tokenizer):
     "Cleans up the tokenized sentence by removing punctuation signs and stopwords"
     import string
     from sklearn.feature_extraction.stop_words import ENGLISH_STOP_WORDS as stopwords
     punctuations = string.punctuation
     tokens=tokenizer(tokens)
     tokens = [str(tok).lower() for tok in tokens if (str(tok).lower() not in stopwords and str(tok).lower() not in punctuations)]     
     return tokens

#Get train.csv and test.csv from "https://www.kaggle.com/c/spooky-author-identification/data"

print('Preparing the training set')
train_data=pd.read_csv("train.csv")
label=train_data.author
authors=list(Counter(label))

texts = list(range(len(train_data)))
tokenizee = spacy.load("en")

tokenizer = Tokenizer(num_words=MAX_NB_WORDS)
for i in range(len(train_data)):
    texts[i]=tokenize(train_data.text[i],tokenizee)
for i in range(len(texts)):
    texts[i] = ' '.join(texts[i])
#sys.exit()
tokenizer.fit_on_texts(texts)
sequences = tokenizer.texts_to_sequences(texts)

word_index = tokenizer.word_index
print('Found %s unique tokens.' % len(word_index))

train = pad_sequences(sequences, maxlen=MAX_SEQUENCE_LENGTH)
labels = label_binarize(label, classes=['EAP', 'HPL', 'MWS'])

# split the data into a training set and a validation set
#indices = np.arange(train.shape[0])
#np.random.shuffle(indices)
#data = train[indices]
#labels = labels[indices]
#num_validation_samples = int(VALIDATION_SPLIT * data.shape[0])
#
#x_train = data[:-num_validation_samples]
#y_train = labels[:-num_validation_samples]
#x_val = data[-num_validation_samples:]
#y_val = labels[-num_validation_samples:]

print('Preparing the test set')
test_data=pd.read_csv("test.csv")

texts = test_data.text

tokenizer.fit_on_texts(texts)
sequences = tokenizer.texts_to_sequences(texts)

test = pad_sequences(sequences, maxlen=MAX_SEQUENCE_LENGTH)

train = [ii for ii in train]
test = [jj for jj in test]
label = label.tolist()

from sklearn.linear_model import SGDClassifier
sgd_clf = SGDClassifier(random_state=42,loss="log")
sgd_clf.fit ( train , label )

scores=sgd_clf.decision_function(test)
scores1=sgd_clf.predict_proba(test)

sys.exit()
print('Making a bag-of-words vocabulary')
vocab_unique=np.load("vocab_unique.npy")
vocab_tfidf=np.load("vocab_fr.npy")
print("Excluding words that are very common/rare and don't contribute much")
vocab_tfidf1=list(range(len(vocab_tfidf)))
vocab_tfidf=[item.tolist() for item in vocab_tfidf]
for i in range(len(vocab_tfidf)):
    vocab_tfidf1[i]=None
    cond=[index for index,value in enumerate(vocab_tfidf[i]) if value==0]
    if sum(vocab_tfidf[i])==0:
        vocab_tfidf1[i]=None
    elif any(x>3 for x in vocab_tfidf[i]): 
        vocab_tfidf1[i]=vocab_tfidf[i]
    else:
        vocab_tfidf1[i]=None
ind=[index for index,value in enumerate(vocab_tfidf1) if value!=None]
words=[vocab_unique[i] for i in ind]

# GloVe embedding data is here:
# http://nlp.stanford.edu/data/glove.6B.zip
print('Indexing word vectors.')

embeddings_index = {}
file_name = os.path.join(GLOVE_DIR, 'glove.42B.300d.txt')
f = open(file_name)
for line in f:
    values = line.split()
    word = values[0]
    for jj in words:
        if jj==word:
            coefs = np.asarray(values[1:], dtype='float32')
            embeddings_index[word] = coefs
            continue
f.close()

print('Found %s word vectors.' % len(embeddings_index))
print('Preparing embedding matrix.')

num_words = min(MAX_NB_WORDS, len(word_index))
embedding_matrix = np.zeros((num_words, EMBEDDING_DIM))
for word, i in word_index.items():
    if i >= MAX_NB_WORDS:
        continue
    embedding_vector = embeddings_index.get(word)
    if embedding_vector is not None:
        # words not found in embedding index will be all-zeros.
        embedding_matrix[i] = embedding_vector

# load pre-trained word embeddings into an Embedding layer
# trainable = False will keep the embeddings fixed
embedding_layer = Embedding(num_words,
                            EMBEDDING_DIM,
                            weights=[embedding_matrix],
                            input_length=MAX_SEQUENCE_LENGTH,
                            trainable=False)

print('Training model ...')
sequence_input = Input(shape=(MAX_SEQUENCE_LENGTH,), dtype='int32')
embedded_sequences = embedding_layer(sequence_input)
x = Conv1D(128, 5, activation='relu')(embedded_sequences)
x = MaxPooling1D(5)(x)
x = Conv1D(128, 5, activation='relu')(x)
x = MaxPooling1D(5)(x)
x = Conv1D(128, 5, activation='relu')(x)
x = GlobalMaxPooling1D()(x)
x = Dense(128, activation='relu')(x)
preds = Dense(len(authors), activation='softmax')(x)

model = Model(sequence_input, preds)
model.compile(loss='categorical_crossentropy',
              optimizer='rmsprop',
              metrics=['acc'])

model.fit(train, labels,
          batch_size=128,
          epochs=10)
#          validation_data=(x_val, y_val))
sys.exit()
scores = model.predict(test, batch_size=128, verbose=1)

ids=test_data.id
ids=np.asarray(["\""+item+"\"" for item in ids])
ids=ids.T
a=np.column_stack((ids,scores))
np.savetxt('submit_keras.csv', a, delimiter=',', fmt="%s")
