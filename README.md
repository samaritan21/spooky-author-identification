This code uses scikit-learn to find stop-words and SpaCy to tokenize sentences 
from the work of three horror authors, Edgar Alan Poe, Mary Shelley, and HP 
Lovecraft.

It then uses GloVe embedding data (http://nlp.stanford.edu/data/glove.6B.zip) to 
create the word-vectors. The word-vectors are then passed on to a deep neural 
network (implememnted in Keras).